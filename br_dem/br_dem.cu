/**
This code (which the amount of data could be reduce (aptex not used anymore,
dtex and entex could be combined)  calculates the intensities along
the axis assuming SE
**/
texture<float, 3, cudaReadModeElementType> emstex; // 3D texture
texture<float, 3, cudaReadModeElementType> tgtex; // 3D texture
texture<float, 3, cudaReadModeElementType> uutex; // 3D texture

texture<float, 1, cudaReadModeElementType> xtex;
texture<float, 1, cudaReadModeElementType> ixtex;
texture<float, 1, cudaReadModeElementType> iytex;
texture<float, 1, cudaReadModeElementType> iztex;

//range of x, y, z axes
__constant__ float xmin;
__constant__ float xmax;
__constant__ float ymin;
__constant__ float ymax;
__constant__ float zmin;
__constant__ float zmax;

__constant__ float tgmin;
__constant__ float tgmax;

__constant__ float uumin;
__constant__ float uumax;

__constant__ float ds; //size of increment
__constant__ int projectionXsize; //x-dimension of the output array
__constant__ int projectionYsize; //y-dimension of the output array

__constant__ int axis;

extern "C" {
    __global__ void demRender(float *out) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx % projectionYsize;
        int xpixel = idx / projectionYsize;

        float3 cp;
        float* ati; //axis to increment
        float target = 1;
        float da = 1.0 * ds;

        switch(axis) {
            case 0: cp.y = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.x;
                    break;
            case 1: cp.x = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.y;
                    break;
            default: cp.x = xpixel / (float) projectionXsize;
                    cp.y = ypixel / (float) projectionYsize;
                    ati = &cp.z;
                    break;
//            default: return;
        }

        *ati = 1 - target; //start at 0

        float ems = 0.0;

        do {
            float uu = tex3D(uutex, cp.x, cp.y, cp.z);

            if (uu > uumin){
              if (uu < uumax){
                float tg = tex3D(tgtex, cp.x, cp.y, cp.z);
                if (tg > tgmin){
                  if (tg < tgmax){
                    ems += tex3D(emstex, cp.x, cp.y, cp.z);
                  }
                }
              }
            }
            *ati += da;
        } while ((*ati < target));

        out[idx] = ems;
    }
}
