#
# .cshrc - executed at start of all shells
#
# add in your .cshrc
# source [PATH]/cuda.csh

# Put the CUDA path and libraries in your .cshrc, e.g., 
#setenv CUDA_HOME /usr/loca/cuda7-5/
#setenv PATH $CUDA_HOME/bin/:$PATH
#setenv LD_LIBRARY_PATH $CUDA_HOME/lib
# Define the path for your LMSAL_HUB. 
setenv CUDA_LIB $LMSAL_HUB/sims_hub/br_hub/br_cuda/


