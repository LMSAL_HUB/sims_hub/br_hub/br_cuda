__constant__ float xlen;
__constant__ float ylen;
__constant__ float zlen;

__constant__ float xmin;
__constant__ float ymin;
__constant__ float zmin;

__constant__ float xmax;
__constant__ float ymax;
__constant__ float zmax;


texture<float, 1, cudaReadModeElementType> xtex;
texture<float, 1, cudaReadModeElementType> ytex;
texture<float, 1, cudaReadModeElementType> ztex;

texture<float, 1, cudaReadModeElementType> ixtex;
texture<float, 1, cudaReadModeElementType> iytex;
texture<float, 1, cudaReadModeElementType> iztex;

texture<float, cudaTextureType3D, cudaReadModeElementType> x;
texture<float, cudaTextureType3D, cudaReadModeElementType> y;
texture<float, cudaTextureType3D, cudaReadModeElementType> z;

__device__ int arrayIndex(int3 a) {

  /*
  Flattens 3D array lookup <a> to 1D index <idx>
  */

  long idx = a.x + xlen * a.y; // + xlen * ylen * a.z;
  return idx;

}

__device__ float3 textureIndex(float3 a) {

  /*
  Converts from real- to index- space coordinates, uses offset to implement slicing
  */

  a.x = fmodf(a.x, xmax - xmin);
  a.y = fmodf(a.y, ymax - ymin);

  a.x = tex1D(ixtex, (a.x - xmin) / (xmax - xmin)) * (xlen-1) + 0.5f;
  a.y = tex1D(iytex, (a.y - ymin) / (ymax - ymin)) * (ylen-1) + 0.5f;
  a.z = tex1D(iztex, (a.z - zmin) / (zmax - zmin)) * (zlen-1) + 0.5f;

  return a;

}
