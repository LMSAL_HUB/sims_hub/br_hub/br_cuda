//#include "$CUDA_LIB/q_factor/q.cuh"

__constant__ float xlen;
__constant__ float ylen;
__constant__ float zlen;

__constant__ float xmin;
__constant__ float ymin;
__constant__ float zmin;

__constant__ float xmax;
__constant__ float ymax;
__constant__ float zmax;

__constant__ float upper;   //Real Z-coordinate for the upper (lesser) bound
__constant__ float lower;   //Real Z-coordinate for the lower (greater) bound
__constant__ float offset;  //Offsets for sliced array lookups
__constant__ float step;    //Increment for tracing field lines

__constant__ float mintheta;  //Cutoff that precludes artifacts
__constant__ float maxlength; //Cutoff that precludes infinite loops

texture<float, 1, cudaReadModeElementType> xtex;
texture<float, 1, cudaReadModeElementType> ytex;
texture<float, 1, cudaReadModeElementType> ztex;

texture<float, 1, cudaReadModeElementType> ixtex;
texture<float, 1, cudaReadModeElementType> iytex;
texture<float, 1, cudaReadModeElementType> iztex;

texture<float, cudaTextureType3D, cudaReadModeElementType> bxc;
texture<float, cudaTextureType3D, cudaReadModeElementType> byc;
texture<float, cudaTextureType3D, cudaReadModeElementType> bzc;

texture<float, cudaTextureType3D, cudaReadModeElementType> ex;
texture<float, cudaTextureType3D, cudaReadModeElementType> ey;
texture<float, cudaTextureType3D, cudaReadModeElementType> ez;


__device__ float dot(float3 a, float3 b) {

  /*
  Dot product
  */

  return a.x * b.x + a.y * b.y + a.z * b.z;

}

__device__ int arrayIndex(int3 a) {

  /*
  Flattens 3D array lookup <a> to 1D index <idx>
  */

  long idx = a.x + xlen * a.y; // + xlen * ylen * a.z;
  return idx;

}

__device__ float3 textureIndex(float3 a) {

  /*
  Converts from real- to index- space coordinates, uses offset to implement slicing
  */

  a.x = fmodf(a.x, xmax - xmin);
  a.y = fmodf(a.y, ymax - ymin);

  a.x = tex1D(ixtex, (a.x - xmin) / (xmax - xmin)) * (xlen-1) + 0.5f;
  a.y = tex1D(iytex, (a.y - ymin) / (ymax - ymin)) * (ylen-1) + 0.5f;
  a.z = tex1D(iztex, (a.z - zmin) / (zmax - zmin)) * (zlen-1) + 0.5f - offset;

  return a;

}

__device__ float parallel(float3 a) {

  float3 b, c;

  a = textureIndex(a);

  b.x = tex3D(bxc, a.x, a.y, a.z);
  b.y = tex3D(byc, a.x, a.y, a.z);
  b.z = tex3D(bzc, a.x, a.y, a.z);

  c.x = tex3D(ex, a.x, a.y, a.z);
  c.y = tex3D(ey, a.x, a.y, a.z);
  c.z = tex3D(ez, a.x, a.y, a.z);

  return dot(b, c);

}

__device__ float theta(float3 a) {

  /*
  Returns the angle of the magetic field at point <a>
  */

  float3 b = textureIndex(a);

  a.x = tex3D(bxc, b.x, b.y, b.z);
  a.y = tex3D(byc, b.x, b.y, b.z);
  a.z = tex3D(bzc, b.x, b.y, b.z);

  return (atan(a.z * a.z / (a.x * a.x + a.y * a.y)));
}

__device__ int direction(float3 a){

  /*
  Returns the direction of the magnetic field
  */

  a = textureIndex(a);

  float z = tex3D(bzc, a.x, a.y, a.z);

  if(z < 0) return 1; else return -1;
}

__device__ float3 takeStep(float3 a, int d) {

  /*
  Takes a step along the field line
  */

  float3 b = textureIndex(a), c;

  c.x = tex3D(bxc, b.x, b.y, b.z);
  c.y = tex3D(byc, b.x, b.y, b.z);
  c.z = tex3D(bzc, b.x, b.y, b.z);

  float f = sqrt(c.x * c.x + c.y * c.y + c.z * c.z);

  a.x += c.x / f * step * d;
  a.y += c.y / f * step * d;
  a.z += c.z / f * step * d;

  return a;

}

__device__ float3 findIntersection(float3 a, int d){

  /*
  Approximates the intersection of a line with a plane

  */

  float3 b = a;

  while((lower < a.z) or (a.z < upper)){
    d = d * .9;
    a = b;
    a = takeStep(a, d);

    if(abs(d) < .0001) break;
  }

  return a;

}

__device__ float4 integrateLine(float3 a, int d) {

  /*
  Executes integration
  */

  if((lower < a.z) or (a.z < upper)) return(make_float4(a.x, a.y, a.z, 0));
  if(theta(a) < mintheta) return(make_float4(a.x, a.y, a.z, 0));

  float e = 0;
  float3 b;

  for(int i = 0; i < maxlength; i++){

    b = a;
    a = takeStep(a, d);
    e += parallel(a);

    if((lower < a.z) or (a.z < upper)){

      a = findIntersection(a, d);
      return(make_float4(a.x, a.y, a.z, e));

    }

  }

  return(make_float4(a.x, a.y, a.z, e));

}

__global__ void integrate(float *d_x, float *d_y, float *d_z, float *d_d, float *d_i, int z) {

  /*
  Traces a line and integrates the parallel component of the electric field along it
  */

  int3 a;
  float3 b;
  bool c = (z == 0);
  int d;

  a.x = (blockIdx.x * blockDim.x) + threadIdx.x;
  a.y = (blockIdx.y * blockDim.y) + threadIdx.y;
  a.z = z;

  long idx = arrayIndex(a);
  /*
  b.x = c ? d_x[idx] : tex1D(xtex, a.x + 0.5f);
  b.y = c ? d_y[idx] : tex1D(ytex, a.y + 0.5f);
  b.z = c ? d_z[idx] : tex1D(ztex, a.z + 0.5f);

  d_d[idx] = c ? d_d[idx] : direction(b);
  d = d_d[idx];

  float4 e = integrateLine(b, d);

  d_x[idx] = e.x;
  d_y[idx] = e.y;
  d_z[idx] = e.z;*/
  d_x[idx] += 1;
  d_y[idx] += 1;
  d_z[idx] += 1;
  d_i[idx] += 1;
}
