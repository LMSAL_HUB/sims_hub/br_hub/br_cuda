
__constant__ float xlen;
__constant__ float ylen;
__constant__ float zlen;

__constant__ float xmin;
__constant__ float ymin;
__constant__ float zmin;

__constant__ float xmax;
__constant__ float ymax;
__constant__ float zmax;

texture<float, 1, cudaReadModeElementType> xtex;
texture<float, 1, cudaReadModeElementType> ytex;
texture<float, 1, cudaReadModeElementType> ztex;

texture<float, 1, cudaReadModeElementType> ixtex;
texture<float, 1, cudaReadModeElementType> iytex;
texture<float, 1, cudaReadModeElementType> iztex;

texture<float, cudaTextureType3D, cudaReadModeElementType> x;
texture<float, cudaTextureType3D, cudaReadModeElementType> y;
texture<float, cudaTextureType3D, cudaReadModeElementType> z;
texture<float, cudaTextureType3D, cudaReadModeElementType> l;

__device__ int arrayIndex(int3 a) {

  /*
  Flattens 3D array lookup <a> to 1D index <idx>
  */

  long idx = a.x + xlen * a.y; // + xlen * ylen * a.z;
  return idx;

}

__device__ float3 textureIndex(float3 a) {

  /*
  Converts from real- to index- space coordinates
  */

  /*
  while(a.x < xmin) a.x += (xmax - xmin);
  while(a.x > xmax) a.x -= (xmax - xmin);
  while(a.y < ymin) a.y += (ymax - ymin);
  while(a.y > ymax) a.y -= (ymax - ymin);
  */

  a.x = fmodf(a.x, xmax - xmin);
  a.y = fmodf(a.y, ymax - ymin);


  a.x = tex1D(ixtex, (a.x - xmin) / (xmax - xmin)) * (xlen-1) + 0.5f;
  a.y = tex1D(iytex, (a.y - ymin) / (ymax - ymin)) * (ylen-1) + 0.5f;
  a.z = tex1D(iztex, (a.z - zmin) / (zmax - zmin)) * (zlen-1) + 0.5f;

  return a;

}

__global__ void connectivity(float* d_c){

  /*
  Generates connectivity map
  */

  int3 a;

  a.x = (blockIdx.x * blockDim.x) + threadIdx.x;
  a.y = (blockIdx.y * blockDim.y) + threadIdx.y;
  a.z = 0;

  long idx = arrayIndex(a);

  a.x = a.x + 0.5f;
  a.y = a.y + 0.5f;
  a.z = a.z + 0.5f;

  float3 b, c;
  float len;

  b.x = tex1D(xtex, a.x + 0.5f);
  b.y = tex1D(ytex, a.y + 0.5f);

  c.x = tex3D(x, a.x, a.y, 0.5f);
  c.y = tex3D(y, a.x, a.y, 0.5f);

  len = tex3D(l, a.x, a.y, 0.5f);

  d_c[idx] = len * sqrt((b.x - c.x) * (b.x - c.x) + (b.y - c.y) * (b.y - c.y));

}


__global__ void alternateConnectivity(float* d_c){

  /*
  Generates connectivity map
  */

  int3 a;

  a.x = (blockIdx.x * blockDim.x) + threadIdx.x;
  a.y = (blockIdx.y * blockDim.y) + threadIdx.y;
  a.z = 0;

  long idx = arrayIndex(a);

  a.x = a.x + 0.5f;
  a.y = a.y + 0.5f;
  a.z = a.z + 0.5f;

  float3 b, c;
  float len;

  b.x = tex1D(xtex, a.x + 0.5f);
  b.y = tex1D(ytex, a.y + 0.5f);

  c.x = tex3D(x, a.x, a.y, 0.5f);
  c.y = tex3D(y, a.x, a.y, 0.5f);

  len = tex3D(l, a.x, a.y, 0.5f);

  d_c[idx] = sqrt((b.x - c.x) * (b.x - c.x) + (b.y - c.y) * (b.y - c.y)) / len;

}
