#include "renderer.cuh"

#define PLANCK 6.626176e-27
#define DCF 1 //density conversion factor
#define CC 3.00e8 // m/s
#define PI 3.1415965
#define SQRTPI 1.772454
#define GRPH 2.38049e-24  // cgs
#define cm2m 1e2
#define cm32m3 1e-6
#define ergd2wd 0.1

texture<float, 3, cudaReadModeElementType> dtex; // 3D texture
texture<float, 3, cudaReadModeElementType> tgtex; // 3D texture //EOSORIG
texture<float, 3, cudaReadModeElementType> uxtex;
texture<float, 3, cudaReadModeElementType> uytex;
texture<float, 3, cudaReadModeElementType> uztex; // 3D texture
texture<float, 3, cudaReadModeElementType> emtex; // 3D texture
texture<float, 2, cudaReadModeElementType> katex; // 2D texture form opacity table 

__constant__ float opatgmin;
__constant__ float opatgrange;

/**
THIS MODULE SOLVE THE RADIATIVE TRANSFER (INTENSITY, SPECTRA WITH AND WITHOUT
OPACITY) FOR TIME DEPENDENCE IONIZATION FOR ANY ANGLE. 

  (em, uu, sqrt(tt))
  x, y, z, are in terms of the split of the box, not of the entire box or the view
 **/
__device__ float3 pointSpecificEmiss(float x, float y, float z, bool iRenderOnly) {
    float em = tex3D(emtex, x, y, z);

    if (iRenderOnly) return make_float3(em, 0, 0);

    float tt = tex3D(tgtex, x, y, z);  //EOSORIG

    float uu =  (
            tex3D(uxtex, x, y, z) * viewVector.x +
            tex3D(uytex, x, y, z) * viewVector.y +
            tex3D(uztex, x, y, z) * viewVector.z);

    return make_float3(em, uu, sqrtf(tt));
}

__device__ float pointSpecificTau(float x, float y, float z) {
    float d2 = tex3D(dtex, x, y, z);                 // cgs 
    float dst = tex3D(dstex, x, y, z);               // cgs
    float tt = tex3D(tgtex, x, y, z);                // SI/cgs (K) 
    
    float tti = (__log10f(tt) - opatgmin) / opatgrange;
    float kk = tex2D(katex, 0, tti);               // lookup opacity cgs

    return dst * (kk * (d2 / GRPH)); 
}

extern "C" {
    __global__ void iRender(float *out, float *tau, bool opacity) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx / projectionXsize;
        int xpixel = idx % projectionXsize;
        float3 positionIncrement = viewVector * ds;

        float3 currentPosition = initialCP(xpixel, ypixel);
        float3 nextPosition = currentPosition + positionIncrement;
        float3 np;

        float emiss = 0;
        float tausum = opacity ? tau[idx] : 0;

        if (currentPosition.x == INFINITY) {
            out[idx] = 0;
            return;
        }

        do {
            np = realToNormalized(currentPosition);
            if (tausum <= 1e2) {
                emiss += pointSpecificEmiss(np.x, np.y, np.z, true).x *
                    expf(-tausum);
            }

            if (opacity) {
                tausum += pointSpecificTau(np.x, np.y, np.z);
            }

            currentPosition += positionIncrement;
         } while (isInSlice(currentPosition));

        if (opacity) tau[idx] = tausum;
        out[idx] = emiss;
    }

    __global__ void ilRender(float *out, float *dnus, float *tau,
                 float nu0, float dopp_width0, int nlamb, bool opacity, int nfreq) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx / projectionXsize;
        int xpixel = idx % projectionXsize;
        float3 positionIncrement = viewVector * ds;

        float3 currentPosition = initialCP(xpixel, ypixel);
        float3 nextPosition = currentPosition + positionIncrement;
        float3 np;

        float3 pointSpecificData;

        float dnu;

        float tausum = opacity ? tau[idx] : 0;
        float dopp_width, shift, phi;

        if (currentPosition.x == INFINITY) {
            out[idx] = 0;
            return;
        }

        do {
            np = realToNormalized(currentPosition);

            if (tausum <= 1e2) {
                pointSpecificData = pointSpecificEmiss(
                        np.x, np.y, np.z, false);
                pointSpecificData.x *= expf(-tausum);

                dopp_width = pointSpecificData.z * dopp_width0;

                dnu = dnus[nfreq];           
                shift = (dnu - nu0 * pointSpecificData.y / CC) / dopp_width;
                phi = __expf(-shift * shift) / (SQRTPI * dopp_width);
                out[idx] += phi * pointSpecificData.x;
            }

            if (opacity) {
                tausum += pointSpecificTau(np.x, np.y, np.z);
            }

            currentPosition += positionIncrement;
         } while (isInSlice(currentPosition));

        if (opacity) tau[idx] = tausum;
        out[idx] *= DCF * DCF * PLANCK * (dnus[nfreq] + nu0) / (4 * PI);
      }
  }
