#include "renderer.cuh"
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <assert.h>

#define CC 3.00e8 // m/s
#define PI 3.1415965
#define SQRTPI 1.772454
#define GRPH 2.38049e-24  // cgs
#define cm2m 1e2
#define cm32m3 1e-6
#define ergd2wd 0.1

/**
This code (which the amount of data could be reduce (aptex not used anymore,
dtex and entex could be combined)  calculates the intensities along
the axis (x, y or z) assuming SE
**/
texture<float, 3, cudaReadModeElementType> dtex;    // 3D texture
texture<float, 3, cudaReadModeElementType> dstex;   // 3D texture
texture<float, 3, cudaReadModeElementType> tgtex;   // 3D texture
texture<float, 3, cudaReadModeElementType> uatex;   // velocity along axis of integration
texture<float, 2, cudaReadModeElementType> atex;    // 2D texture
texture<float, 3, cudaReadModeElementType> entex;   // 3D texture //EOSORIG
texture<float, 2, cudaReadModeElementType> katex;   // 2D texture //EOSORIG
texture<float, 3, cudaReadModeElementType> m2tex;   // 3D texture 
texture<float, 3, cudaReadModeElementType> modbtex; // 3D texture 

__constant__ float enmin;
__constant__ float enrange;

__constant__ float tgmin;
__constant__ float tgrange;

__constant__ float opatgmin;
__constant__ float opatgrange;

__constant__ float modbmin;
__constant__ float modbrange;

__constant__ int axis;
__constant__ bool reverse; //go along axis in reverse direction

#define X_AXIS 120
#define Y_AXIS 121
#define Z_AXIS 122

__device__ float3 pointSpecificEmiss(float x, float y, float z, bool iRenderOnly) {
    float nh = tex3D(dtex, x, y, z) / GRPH;            // cgs
    float dst = tex3D(dstex, x, y, z) / cm2m;          // SI (by / cm2m)
    float tt = tex3D(tgtex, x, y, z);                  // SI/cgs (K) 
    float en = tex3D(entex, x, y, z);                  // cgs
    float edi = (__log10f(en) - enmin) / enrange;  // temperature, edensity lookup values
    float tti = (__log10f(tt) - tgmin) / tgrange;

    float g = tex2D(atex, edi, tti) * ergd2wd;         // lookup g SI including ne * nh

    if (iRenderOnly) return make_float3((en * (nh * g)) * dst , 0, 0);

    float uu = (tex3D(uatex, x, y, z) * (reverse ? 1 : -1)); // SI

    return make_float3((en * (nh  * g)) * dst, uu, sqrtf(tt));
}


__device__ float pointSpecificTau(float x, float y, float z) {
    float d2 = tex3D(dtex, x, y, z);                 // cgs 
    float dst = tex3D(dstex, x, y, z);               // cgs 
    float tt = tex3D(tgtex, x, y, z);                // SI/cgs (K) 
    
    float tti = (__log10f(tt) - opatgmin) / opatgrange ;
    float kk = tex2D(katex, 0.5, tti);               // lookup opacity cgs but in Mm

    return dst * (kk * (d2 / GRPH)); 
}


__device__ float3 pointSpecificMIT(float x, float y, float z, bool iRenderOnly) {
    float modb = tex3D(modbtex, x, y, z);              // Gauss
    float modbi = (modb - modbmin) / modbrange;
    float nh = tex3D(dtex, x, y, z) / GRPH;            // cgs
    float dst = tex3D(dstex, x, y, z) / cm2m;          // SI (by / cm2m)
    float tt = tex3D(tgtex, x, y, z);                  // SI/cgs (K) 
    float en = tex3D(entex, x, y, z);                  // cgs
    float edi = (__log10f(en) - enmin) / enrange;  // temperature, edensity lookup values
    float tti = (__log10f(tt) - tgmin) / tgrange;

    float g = tex3D(m2tex, modbi, tti, edi) * ergd2wd;         // lookup g SI including ne * nh

    if (iRenderOnly) return make_float3((en * (nh * g)) * dst , 0, 0);

    float uu = (tex3D(uatex, x, y, z) * (reverse ? 1 : -1)); // SI

    return make_float3((en * (nh  * g)) * dst, uu, sqrtf(tt));

}



extern "C" {
    __global__ void iRender(float *out, float *tau, bool opacity, bool do_mit) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx % projectionYsize;
        int xpixel = idx / projectionYsize;

        float3 cp;
        float* ati; //axis to increment
        float da = (reverse ? 1.0 : -1.0)*ds; // / nsteps;
        float target = reverse ? 1 : 0;

        switch(axis) {
            case 0: cp.y = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.x;
                    break;
            case 1: cp.x = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.y;
                    break;
            default: cp.x = xpixel / (float) projectionXsize;
                    cp.y = ypixel / (float) projectionYsize;
                    ati = &cp.z;
                    break;
        }

        *ati = 1 - target; //start at either 0 or 1
        
        out[idx] = 0;
        float tausum = opacity ? tau[idx] : 0;
        
        do {
            if (tausum <= 1e2) {
                if (do_mit) {    
                    out[idx] += pointSpecificMIT(cp.x, cp.y, cp.z, true).x *
                        expf(-tausum);
                }else{
                    out[idx] += pointSpecificEmiss(cp.x, cp.y, cp.z, true).x * 
                        expf(-tausum);                
                }
            }
            if (opacity) {
                tausum += pointSpecificTau(cp.x, cp.y, cp.z);
            }
            
            *ati += da;
        } while (reverse ? (*ati < target) : (*ati > target));
        
        if (opacity) tau[idx] = tausum;
    }

    __global__ void ilRender(float *out, float *dnus, float *tau,
                 float nu0, float dopp_width0, int nlamb, bool opacity, int nfreq, bool do_mit) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx % projectionYsize;
        int xpixel = idx / projectionYsize;

        float3 cp;
        float* ati; //axis to increment
        float da = (reverse ? 1.0 : -1.0) * ds; /// nsteps;
        float target = reverse ? 1 : 0;

        switch(axis) {
            case 0: cp.y = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.x;
                    break;
            case 1: cp.x = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.y;
                    break;
            default: cp.x = xpixel / (float) projectionXsize;
                    cp.y = ypixel / (float) projectionYsize;
                    ati = &cp.z;
                    break;
        }

        *ati = 1 - target;

        float3 pointSpecificData;
        float dopp_width, shift, phi;
        float tausum = opacity ? tau[idx] : 0;

        out[idx] = 0;

        do {
            if (tausum <= 1e2) {
               
                if (do_mit) {
                    pointSpecificData = pointSpecificMIT(cp.x, cp.y, cp.z, false);
                }else{ 
                    pointSpecificData = pointSpecificEmiss(
                        cp.x, cp.y, cp.z, false);
                }
                dopp_width = pointSpecificData.z * dopp_width0;

                pointSpecificData.x *= expf(-tausum);

                shift = (dnus[nfreq] - nu0 * pointSpecificData.y / CC) / dopp_width;
                phi = __expf(-shift * shift) / (SQRTPI * dopp_width);
                out[idx] += phi * pointSpecificData.x;
            }

            if (opacity) {
                tausum += pointSpecificTau(cp.x, cp.y, cp.z);
            }
            
            *ati += da;
        } while (reverse ? (*ati < target) : (*ati > target));

        if (opacity) tau[idx] = tausum;
    }
}
