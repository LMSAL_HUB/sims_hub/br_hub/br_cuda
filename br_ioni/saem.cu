#include "renderer.cuh"
/**
This code (which needs to be fixed since has pieces from
saioni.cu)  calculates the intensities along
the axis (x, y or z) for ooe module
**/

#define PLANCK 6.626176e-27
#define DCF 1 //density conversion factor
#define CC 3.00e8 // m/s
#define PI 3.1415965
#define SQRTPI 1.772454
#define GRPH 2.38049e-24  // cgs
#define cm2m 1e2
#define cm32m3 1e-6
#define ergd2wd 0.1

texture<float, 3, cudaReadModeElementType> dtex; // 3D texture
texture<float, 3, cudaReadModeElementType> dstex; // 3D texture
texture<float, 3, cudaReadModeElementType> tgtex; // 3D texture 
texture<float, 3, cudaReadModeElementType> uatex; // velocity along axis of integration
texture<float, 3, cudaReadModeElementType> katex; // 3D texture 
texture<float, 2, cudaReadModeElementType> emtex; // 2D texture form opacity table 

/**
THIS MODULE SOLVE THE RADIATIVE TRANSFER (INTENSITY, SPECTRA WITH AND WITHOUT
OPACITY) FOR ONE SPECIFIC AXIS. USED IN SINGLEAXIS.PY FOR TIME DEPENDENCE IONIZATION

**/

//__constant__ float dmin; //EOSORIG
//__constant__ float drange; //EOSORIG

//__constant__ float emin; //EOSORIG
//__constant__ float erange; //EOSORIG

__constant__ float opatgmin;
__constant__ float opatgrange;

__constant__ char axis;
__constant__ bool reverse; //go along axis in reverse direction

__constant__ int projectionXsize;
__constant__ int projectionYsize;

#define X_AXIS 0
#define Y_AXIS 1
#define Z_AXIS 2

__device__ float3 pointSpecificEmiss(float x, float y, float z, bool iRenderOnly) {
    float em = tex3D(emtex, x, y, z);

    if (iRenderOnly) return make_float3(em, 0, 0);

    float tt = tex3D(tgtex, x, y, z);  //EOSORIG
    
    float uu = (tex3D(uatex, x, y, z) * (reverse ? -1 : 1));

    return make_float3(em, uu, sqrtf(tt));

}

__device__ float pointSpecificTau(float x, float y, float z) {
    float d2 = tex3D(dtex, x, y, z);                 // cgs 
    float dst = tex3D(dstex, x, y, z);               // cgs
    float tt = tex3D(tgtex, x, y, z);                // SI/cgs (K) 
    
    float tti = (__log10f(tt) - opatgmin) / opatgrange;
    float kk = tex2D(katex, 0, tti);               // lookup opacity cgs

    return dst * (kk * (d2 / GRPH)); 
}

extern "C" {
    __global__ void iRender(float *out, float *tau, int opacity) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx % projectionYsize;
        int xpixel = idx / projectionYsize;

        float3 cp;
        float* ati; //axis to increment
        float da = (reverse ? 1.0 : -1.0)*ds; // / nsteps;
        float target = reverse ? 1 : 0;

        switch(axis) {
            case 0: cp.y = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.x;
                    break;
            case 1: cp.x = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.y;
                    break;
            default: cp.x = xpixel / (float) projectionXsize;
                    cp.y = ypixel / (float) projectionYsize;
                    ati = &cp.z;
                    break;
        }

        *ati = 1 - target; //start at either 0 or 1

        float emiss = 0;
        float tausum = 0;
        if (opacity==1) tausum = tau[idx];

        do {
            if (tausum <= 1e2) {
                emiss += pointSpecificEmiss(cp.x, cp.y, cp.z, true).x *
                    expf(-tausum);
            }

            if (opacity == 1) {
                tausum += pointSpecificTau(cp.x, cp.y, cp.z);
            }

            *ati += da;
        } while (reverse ? (*ati > target) : (*ati < target));

        if (opacity == 1) tau[idx] = tausum;
        out[idx] = emiss;
    }

    __global__ void ilRender(float *out, float *dnus, float *tau,
                 float nu0, float dopp_width0, int nlamb, int opacity, int nfreq) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx % projectionYsize;
        int xpixel = idx / projectionYsize;

        float3 cp;
        float* ati; //axis to increment
        float da = (reverse ? 1.0 : -1.0)*ds; // / nsteps;
        float target = reverse ? 1 : 0;

        switch(axis) {
            case 0: cp.y = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.x;
                    break;
            case 1: cp.x = xpixel / (float) projectionXsize;
                    cp.z = ypixel / (float) projectionYsize;
                    ati = &cp.y;
                    break;
            default: cp.x = xpixel / (float) projectionXsize;
                    cp.y = ypixel / (float) projectionYsize;
                    ati = &cp.z;
                    break;
        }

        *ati = 1 - target;

        float3 pointSpecificData;
        float dnu;
        float tausum = opacity ? tau[idx] : 0;
        float dopp_width, shift, phi;
        out[idx] = 0;

        do {
            if (tausum <= 1e2) {
                pointSpecificData = pointSpecificEmiss(
                        cp.x, cp.y, cp.z, false);

                dopp_width = pointSpecificData.z * dopp_width0;

                pointSpecificData.x *= expf(-tausum);

                dnu = dnus[nfreq];
                shift = (dnu - nu0 * pointSpecificData.y / CC) / dopp_width;
                phi = __expf(-shift * shift) / (SQRTPI * dopp_width);
                out[idx] += phi * pointSpecificData.x;
            }

            if (opacity == 1) {
                tausum += pointSpecificTau(cp.x, cp.y, cp.z);
            }

            *ati += da;
        } while (reverse ? (*ati < target) : (*ati > target));
        
        out[idx] *= DCF * DCF * PLANCK * (dnus[nfreq] + nu0) / (4 * PI);

        if (opacity == 1) tau[idx] = tausum;
      }
    }
