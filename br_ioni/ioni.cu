#include "renderer.cuh"

#define CC 3.00e8
#define PLANCK 6.626176e-27
#define PI 3.1415965
#define SQRTPI 1.772454
#define GRPH 2.27e-24
// #define DCF 1e7 //density conversion factor
#define DCF 1 //density conversion factor

texture<float, 3, cudaReadModeElementType> dtex; // 3D density texture
texture<float, 3, cudaReadModeElementType> uxtex;
texture<float, 3, cudaReadModeElementType> uytex;
texture<float, 3, cudaReadModeElementType> uztex; // 3D x, y, z vel textures
texture<float, 2, cudaReadModeElementType> atex; // transfer function 
texture<float, 3, cudaReadModeElementType> entex; // electron density 
texture<float, 3, cudaReadModeElementType> tgtex; // temperature 
texture<float, 2, cudaReadModeElementType> katex; // opacity texture //EOSORIG

//__constant__ float dmin;//EOSORIG
//__constant__ float drange;//EOSORIG

//__constant__ float emin;//EOSORIG
//__constant__ float erange;//EOSORIG

__constant__ float enmin;
__constant__ float enrange;

__constant__ float tgmin;
__constant__ float tgrange;

__constant__ float opatgmin;
__constant__ float opatgrange;

/**
THIS MODULE SOLVE THE RADIATIVE TRANSFER (INTENSITY, SPECTRA WITH AND WITHOUT
OPACITY) WITHOUT TIME DEPENDENCE IONIZATION USED IN __INIT__.PY FOR ANY ANGLE. 

  (en * en * g, uu, sqrt(tt))
  x, y, z is a slice-normalized, scaled vector (from realToNormalized())
  if iRenderOnly is true, then skip calculation of uu and tt
 **/
 
__device__ float3 pointSpecificEmiss(float x, float y, float z, bool iRenderOnly) {
    float nh = tex3D(dtex, x, y, z) / GRPH;            // cgs
    float dst = tex3D(dstex, x, y, z) / cm2m;          // SI 
    float tt = tex3D(tgtex, x, y, z);                  // SI/cgs (K) 
    float en = tex3D(entex, x, y, z);                  // cgs
    float edi = (en - enmin) / enrange;  // temperature, edensity lookup values
    float tti = (__log10f(tt) - tgmin) / tgrange;

    float g = tex2D(atex, edi, tti) * ergd2wd;         // lookup g SI including ne * nh

    if (iRenderOnly) return make_float3((en * (nh * g)) * dst , 0, 0);

    float uu = (tex3D(uatex, x, y, z) * (reverse ? 1 : -1)); // SI

    return make_float3((en * (nh  * g)) * dst, uu, sqrtf(tt));

    float uu = (
            tex3D(uxtex, x, y, z) * viewVector.x +
            tex3D(uytex, x, y, z) * viewVector.y +
            tex3D(uztex, x, y, z) * viewVector.z);

    return make_float3((en * (nh  * g)) * dst, uu, sqrtf(tt));
}

/**
   Calculates the increment of opacity for a specific point
*/
__device__ float pointSpecificTau(float x, float y, float z) {
    float d2 = tex3D(dtex, x, y, z);                 // cgs 
    float dst = tex3D(dstex, x, y, z);               // cgs
    float tt = tex3D(tgtex, x, y, z);                // SI/cgs (K) 
    
    float tti = (__log10f(tt) - opatgmin) / opatgrange;
    float kk = tex2D(katex, 0, tti);               // lookup opacity cgs

    return dst * (kk * (d2 / GRPH)); 
}

extern "C" {
    __global__ void iRender(float *out, float *tau, bool opacity) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx / projectionXsize;
        int xpixel = idx % projectionXsize;

        float3 positionIncrement = viewVector * ds;
        float3 currentPosition = initialCP(xpixel, ypixel);
        float3 np; //slice-normalized, scaled position

        float emiss = 0;
        float tausum = opacity ? tau[idx] : 0;

        if (currentPosition.x == INFINITY) {
            out[idx] = 0;
            return;
        }

        do {
            np = realToNormalized(currentPosition);

            if (tausum <= 1e2) {
                emiss += pointSpecificEmiss(np.x, np.y, np.z, true).x *
                    expf(-tausum);
            }

            if (opacity) {
                tausum += pointSpecificTau(np.x, np.y, np.z);
            }

            currentPosition += positionIncrement;
        } while (isInSlice(currentPosition));

        if (opacity) tau[idx] = tausum;
        out[idx] = emiss;
    }

    __global__ void ilRender(float *out, float *dnus, float *tau,
                    float nu0, float dopp_width0, int nlamb, bool opacity, int nfreq) {
        int idx = blockIdx.x * blockDim.x + threadIdx.x;
        if (idx > projectionXsize * projectionYsize) return;
        int ypixel = idx / projectionXsize;
        int xpixel = idx % projectionXsize;
        float3 positionIncrement = viewVector * ds;

        float3 currentPosition = initialCP(xpixel, ypixel);
        float3 np;

        float3 pointSpecificData;

        float dnu;

        float tausum = opacity ? tau[idx] : 0;

        float dopp_width, shift, phi;

        out[idx] = 0;

        if (currentPosition.x == INFINITY) return;

        do {
            np = realToNormalized(currentPosition);

            if (tausum <= 1e2) {
                pointSpecificData = pointSpecificEmiss(
                        np.x, np.y, np.z, false);

                dopp_width = pointSpecificData.z * dopp_width0;

                pointSpecificData.x *= expf(-tausum);

                dnu = dnus[nfreq];
                shift = (dnu - nu0 * pointSpecificData.y / CC) / dopp_width;
                phi = __expf(-shift * shift) / (SQRTPI * dopp_width);
                out[idx] += phi * pointSpecificData.x;
            }

            if (opacity) {
                tausum += pointSpecificTau(np.x, np.y, np.z);
            }

            currentPosition += positionIncrement;
        } while (isInSlice(currentPosition));

        out[idx] *= DCF * DCF * PLANCK * (dnus[nfreq] + nu0) / (4 * PI);

        if (opacity) tau[idx] = tausum;
    }
}
