# LMSAL_HUB/sims_hub/br_hub/br_cuda

Is a library for solar physics focused on interfacing with Bifrost numerical 
codes, analysis tools, projects and visualization tools for numerical 
simulations from the Lockheed Martin Solar and Astrophysical Lab (LMSAL).

The library is a collection of different cuda code with varying degrees of 
portability and usefulness. Some of these tools will be used in br_intcu, 
br_py and br_topocu. 

# Dependencies
* [CUDA](https://developer.nvidia.com/cuda-zone)


# Installation
Next, use git to grab the latest version of sims_hub:

      mkdir -p LMSAL_HUB/sims_hub/br_hub
      cd LMSAL_HUB/sims_hub/br_hub
      git clone https://gitlab.com/LMSAL_HUB/sims_hub/br_hub/br_cuda.git

1) Install CUDA: Follow the download and installsteps in http://developer.nvidia.com/cuda-downloads  

add in the .cshrc: 

       setenv LMSAL_HUB path
       source $LMSAL_HUB/sims_hub/br_hub/br_cuda/cuda.csh

 